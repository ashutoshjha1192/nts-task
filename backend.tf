terraform {
  backend "s3" {
    # Replace this with your bucket name!
    bucket         = "terraform-state"
    key            = "terraform.tfstate"
    region         = "us-east-1"
    encrypt        = true
  }
}
