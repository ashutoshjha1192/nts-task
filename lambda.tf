locals{
  lambda_zip_location = "output/getPublicBucket.zip"
}

data "archive_file" "init" {
  type        = "zip"
  source_file = "getPublicBucket.js"
  output_path = "${local.lambda_zip_location}"
}


resource "aws_lambda_function" "test_lambda" {
  filename      = "${local.lambda_zip_location}"
  function_name = "getPublicBucket"
  role          = "${aws_iam_role.lambda_role.arn}"
  handler       = "getPublicBucket.handler"

  # The filebase64sha256() function is available in Terraform 0.11.12 and later
  # For Terraform 0.11.11 and earlier, use the base64sha256() function and the file() function:
  # source_code_hash = "${base64sha256(file("lambda_function_payload.zip"))}"
  source_code_hash = "${filebase64sha256(local.lambda_zip_location)}"

  runtime = "nodejs12.x"
}
